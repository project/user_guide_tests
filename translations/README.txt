This directory contains PO files for each language that is used in tests and
screenshot generation. All PO files in a given language directory will be
imported when a new language is enabled in the test run.

When adding a new language, download files from
https://localize.drupal.org/download for that language for the latest versions
of the following projects:

- Admin Toolbar
- Honey (theme)
- Drupal core
