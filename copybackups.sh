#!/bin/bash

# Run this script from a directory containing backup files from a test run,
# such as test12345678/backups. The argument is the destination directory,
# such as /home/username/git/user_guide_tests/backups/ca (for Catalan).

for fname in $(ls)
do
    cp $fname/*.gz $1/$fname
done
