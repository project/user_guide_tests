<?php

namespace Drupal\Tests\user_guide_tests\FunctionalJavascript;

use Drupal\backup_migrate\Core\Plugin\PluginBase;

/**
 * Filter that excludes empty directories from file backups.
 */
class FileExcludeEmptyDirFilter extends PluginBase {

  /**
   * Excludes paths that are empty directories.
   *
   * @param string $path
   *   Path to check.
   * @param array $params
   *   Ignored.
   *
   * @return string|null
   *   $path if it's not an empty directory; NULL if it is.
   */
  public function beforeFileBackup($path, array $params = []) {
    // If it's not a directory at all, return it.
    if (!is_dir($path)) {
      return $path;
    }

    // If we can't read it, return NULL.
    if (!is_readable($path)) {
      return NULL;
    }

    if (count(scandir($path)) > 2) {
      return $path;
    }

    return NULL;
  }

}
