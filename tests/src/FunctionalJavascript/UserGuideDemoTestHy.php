<?php

namespace Drupal\Tests\user_guide_tests\FunctionalJavascript;

/**
 * Builds the demo site for the User Guide, Armenian, with screenshots.
 *
 * See README.txt file in the module directory for more information about
 * making screenshots.
 *
 * @group UserGuide
 */
class UserGuideDemoTestHy extends UserGuideDemoTestBase {

  /**
   * Non-override of UserGuideDemoTestBase::runList.
   *
   * If you want to run only some chapters, or want to make backups, change
   * the name of this variable (locally and temporarily) to $runList, and then
   * change 'skip' to one of the other values for each chapter you want to run.
   * See UserGuideDemoTestBase::runList for more information.
   *
   * @var array
   */
  protected $notRunList = [
    'doPrefaceInstall' => 'skip',
    'doBasicConfig' => 'skip',
    'doBasicPage' => 'skip',
    'doContentStructure' => 'skip',
    'doUserAccounts' => 'skip',
    'doBlocks' => 'skip',
    'doViews' => 'skip',
    'doMultilingualSetup' => 'skip',
    'doTranslating' => 'skip',
    'doExtending' => 'skip',
    'doPreventing' => 'skip',
    'doSecurity' => 'skip',
  ];

  /**
   * Strings and other information to input into the demo site.
   *
   * This information is translated into other languages in the
   * specific-language test classes.
   *
   * @var array
   */
  protected $demoInput = [
    // Default and second languages for the site.
    'first_langcode' => 'hy',
    'second_langcode' => 'en',

    // Basic site information.
    'site_name' => 'Anytown Farmers Market',
    'site_slogan' => 'Farm Fresh Food',
    'site_mail' => 'info@example.com',
    'site_default_country' => 'US',
    'date_default_timezone' => 'America/Los_Angeles',

    // Home page content item.
    'home_title' => 'Home',
    'home_body' => "<p>Welcome to City Market - your neighborhood farmers market!</p><p>Open: Sundays, 9 AM to 2 PM, April to September</p><p>Location: Parking lot of Trust Bank, 1st & Union, downtown</p>",
    'home_summary' => 'Opening times and location of City Market',
    'home_path' => '/home',
    'home_revision_log_message' => 'Updated opening hours',

    // Translation of Home page content item into second language.
    'home_title_translated' => 'P?gina principal',
    'home_body_translated' => "<p>Bienvenido al mercado de la ciudad - ?el mercado de agricultores de tu barrio!</p></p>Horario: Domingos de 9:00 a 14:00. Desde Abril a Septiembre Lugar: parking del Banco Trust n?mero 1. En el centro de la ciudad</p>",
    'home_path_translated' => '/pagina-principal',

    // About page content item.
    'about_title' => 'About',
    'about_body' => "<p>City Market started in April 1990 with five vendors.</p><p>Today, it has 100 vendors and an average of 2000 visitors per day.</p>",
    'about_path' => '/about',
    'about_description' => 'History of the market',

    // Vendor content type settings. Type name and machine name are also
    // used for the Vendor role.
    'vendor_type_name' => 'Vendor',
    'vendor_type_machine_name' => 'vendor',
    'vendor_type_description' => 'Information about a vendor',
    'vendor_type_title_label' => 'Vendor name',
    'vendor_field_url_label' => 'Vendor URL',
    'vendor_field_url_machine_name' => 'vendor_url',
    'vendor_field_image_label' => 'Main image',
    'vendor_field_image_machine_name' => 'main_image',
    'vendor_field_image_directory' => 'vendors',

    // Vendor 1 content item and user account.
    'vendor_1_title' => 'Happy Farm',
    'vendor_1_path' => '/vendors/happy_farm',
    'vendor_1_summary' => 'Happy Farm grows vegetables that you will love.',
    'vendor_1_body' => '<p>Happy Farm grows vegetables that you will love.</p><p>We grow tomatoes, carrots, and beets, as well as a variety of salad greens.</p>',
    'vendor_1_url' => 'http://happyfarm.com',
    'vendor_1_email' => 'happy@example.com',

    // Vendor 2 content item and user account.
    'vendor_2_title' => 'Sweet Honey',
    'vendor_2_path' => '/vendors/sweet_honey',
    'vendor_2_summary' => 'Sweet Honey produces honey in a variety of flavors throughout the year.',
    'vendor_2_body' => '<p>Sweet Honey produces honey in a variety of flavors throughout the year.</p><p>Our varieties include clover, apple blossom, and strawberry.</p>',
    'vendor_2_url' => 'http://sweethoney.com',
    'vendor_2_email' => 'honey@example.com',

    // Recipe content type settings.
    'recipe_type_name' => 'Recipe',
    'recipe_type_machine_name' => 'recipe',
    'recipe_type_description' => 'Recipe submitted by a vendor',
    'recipe_type_title_label' => 'Recipe name',
    'recipe_field_image_directory' => 'recipes',
    'recipe_field_ingredients_label' => 'Ingredients',
    'recipe_field_ingredients_machine_name' => 'ingredients',
    'recipe_field_ingredients_help' => 'Enter ingredients that site visitors might want to search for',
    'recipe_field_submitted_label' => 'Submitted by',
    'recipe_field_submitted_machine_name' => 'submitted_by',
    'recipe_field_submitted_help' => 'Choose the vendor that submitted this recipe',

    // Recipe ingredients terms added.
    'recipe_field_ingredients_term_1' => 'Butter',
    'recipe_field_ingredients_term_2' => 'Eggs',
    'recipe_field_ingredients_term_3' => 'Milk',
    'recipe_field_ingredients_term_4' => 'Carrots',

    // Recipe 1 content item.
    'recipe_1_title' => 'Green Salad',
    'recipe_1_path' => '/recipes/green_salad',
    'recipe_1_body' => 'Chop up your favorite vegetables and put them in a bowl.',
    'recipe_1_ingredients' => 'Carrots, Lettuce, Tomatoes, Cucumbers',

    // Recipe 2 content item.
    'recipe_2_title' => 'Fresh Carrots',
    'recipe_2_path' => '/recipes/carrots',
    'recipe_2_body' => 'Serve multi-colored carrots on a plate for dinner.',
    'recipe_2_ingredients' => 'Carrots',

    // Image style.
    'image_style_label' => 'Extra medium (300x200)',
    'image_style_machine_name' => 'extra_medium_300x200',

    // Hours and location block.
    'hours_block_description' => 'Hours and location block',
    'hours_block_title' => 'Hours and location',
    'hours_block_title_machine_name' => 'hours_location',
    'hours_block_body' => "<p>Open: Sundays, 9 AM to 2 PM, April to September</p><p>Location: Parking lot of Trust Bank, 1st & Union, downtown</p>",

    // Vendors view.
    'vendors_view_title' => 'Vendors',
    'vendors_view_machine_name' => 'vendors',
    'vendors_view_path' => 'vendors',

    // Recipes view.
    'recipes_view_title' => 'Recipes',
    'recipes_view_machine_name' => 'recipes',
    'recipes_view_path' => 'recipes',
    'recipes_view_ingredients_label' => 'Find recipes using...',
    'recipes_view_block_display_name' => 'Recent recipes',
    'recipes_view_block_title' => 'New recipes',

    // Recipes view translated.
    'recipes_view_title_translated' => 'Recetas',
    'recipes_view_submit_button_translated' => 'Applicar',
    'recipes_view_ingredients_label_translated' => 'Encontrar recetas usando...',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    // Remove this entire method when we're ready to start running these tests.
    // https://www.drupal.org/project/user_guide/issues/3277383
    $this->markTestSkipped('Skipping the HY tests until translations are complete');
  }

}
