<?php

namespace Drupal\Tests\user_guide_tests\FunctionalJavascript;

/**
 * Builds the demo site for the User Guide, Ukranian, with screenshots.
 *
 * See README.txt file in the module directory for more information about
 * making screenshots.
 *
 * @group UserGuide
 */
class UserGuideDemoTestUk extends UserGuideDemoTestBase {

  /**
   * Non-override of UserGuideDemoTestBase::runList.
   *
   * If you want to run only some chapters, or want to make backups, change
   * the name of this variable (locally and temporarily) to $runList, and then
   * change 'skip' to one of the other values for each chapter you want to run.
   * See UserGuideDemoTestBase::runList for more information.
   *
   * @var array
   */
  protected $notRunList = [
    'doPrefaceInstall' => 'skip',
    'doBasicConfig' => 'skip',
    'doBasicPage' => 'skip',
    'doContentStructure' => 'skip',
    'doUserAccounts' => 'skip',
    'doBlocks' => 'skip',
    'doViews' => 'skip',
    'doMultilingualSetup' => 'skip',
    'doTranslating' => 'skip',
    'doExtending' => 'skip',
    'doPreventing' => 'skip',
    'doSecurity' => 'skip',
  ];

  /**
   * {@inheritdoc}
   */
  protected $demoInput = [
    'first_langcode' => "uk",
    'second_langcode' => "en",

    'site_name' => "Фермерський ярмарок міста N",
    'site_slogan' => "Свіжа їжа з фермерських господарств",
    'site_mail' => "info@example.com",
    'site_default_country' => "UA",
    'date_default_timezone' => "Europe/Kyiv",

    'home_title' => "Головна",
    'home_body' => "<p>Ласкаво просимо на міський ярмарок – продукти від найближчих фермерських господарств!</p><p>Години роботи: Неділя, з 9.00 до 14.00, з квітня по вересень</p><p>Адреса: Парковка Траст-банку, перетин Першої.та Соборної вулиць, Центр</p>",
    'home_summary' => "Години роботи та розташування міського ярмарку",
    'home_path' => "/home",
    'home_revision_log_message' => "Оновлені години роботи",

    'home_title_translated' => "Home",
    'home_body_translated' => "<p>Welcome to City Market - your neighborhood farmers market!</p><p>Open: Sundays, 9 AM to 2 PM, April to September</p><p>Location: Parking lot of Trust Bank, 1st & Union, downtown</p>",
    'home_path_translated' => "/home",

    'about_title' => "Про ярмарок",
    'about_body' => "<p> Міський ярмарок почав проводитись в квітні 1990 року і мав п'ять постачальниками.</p><p> Сьогодні він має 100 постачальників і в середньому 2000 відвідувачів на день.</p>",
    'about_path' => "/about",
    'about_description' => "Історія ярмарку",

    'vendor_type_name' => "Постачальник",
    'vendor_type_machine_name' => "vendor",
    'vendor_type_description' => "Відомості про постачальників",
    'vendor_type_title_label' => "Ім’я постачальника",
    'vendor_field_url_label' => "URL постачальника",
    'vendor_field_url_machine_name' => "vendor_url",
    'vendor_field_image_label' => "Основне зображення",
    'vendor_field_image_machine_name' => "main_image",
    'vendor_field_image_directory' => "vendors",

    'vendor_1_title' => "Щаслива ферма",
    'vendor_1_path' => "/vendors/happy_farm",
    'vendor_1_summary' => "Щаслива ферма вирощує овочі, які вам сподобаються.",
    'vendor_1_body' => "<p>Щаслива ферма вирощує овочі, які вам сподобаються.</p><p>Ми вирощуємо помідори, моркву та буряк, а також різноманітну салатову зелень.</p>",
    'vendor_1_url' => "http://happyfarm.com",
    'vendor_1_email' => "happy@example.com",

    'vendor_2_title' => "Солодкий мед",
    'vendor_2_path' => "/vendors/sweet_honey",
    'vendor_2_summary' => "Солодкий мед виробляє мед з різноманітними смаками протягом року.",
    'vendor_2_body' => "<p>Солодкий мед виробляє мед з різноманітними смаками протягом року.</p><p>Ми пропонуємо мед з конюшини, яблуневого цвіту, полуниці.</p>",
    'vendor_2_url' => "http://sweethoney.com",
    'vendor_2_email' => "honey@example.com",

    'recipe_type_name' => "Рецепт",
    'recipe_type_machine_name' => "recipe",
    'recipe_type_description' => "Рецепт від постачальника",
    'recipe_type_title_label' => "Назва рецепту",
    'recipe_field_image_directory' => "recipes",
    'recipe_field_ingredients_label' => "Інгредієнти",
    'recipe_field_ingredients_machine_name' => "ingredients",
    'recipe_field_ingredients_help' => "Введіть інгредієнти, які можуть шукати відвідувачі сайту",
    'recipe_field_submitted_label' => "Надіслано",
    'recipe_field_submitted_machine_name' => "submitted_by",
    'recipe_field_submitted_help' => "Оберіть постачальника, що надіслав рецепт",

    'recipe_field_ingredients_term_1' => "Масло",
    'recipe_field_ingredients_term_2' => "Яйця",
    'recipe_field_ingredients_term_3' => "Молоко",
    'recipe_field_ingredients_term_4' => "Морква",

    'recipe_1_title' => "Зелений салат",
    'recipe_1_path' => "/recipes/green_salad",
    'recipe_1_body' => "Наріжте свої улюблені овочі і покладіть їх у чашу.",
    'recipe_1_ingredients' => "Морква",

    'recipe_2_title' => "Свіжі моркви",
    'recipe_2_path' => "/recipes/carrots",
    'recipe_2_body' => "Подайте різнокольорову моркву на тарілці до вечері.",
    'recipe_2_ingredients' => "Морква",

    'image_style_label' => "Екстра-середній (300x200)",
    'image_style_machine_name' => "extra_medium_300x200",

    'hours_block_description' => "Години та блок розташування",
    'hours_block_title' => "Години та розташування",
    'hours_block_title_machine_name' => "hours_location",
    'hours_block_body' => "<p>Години роботи: Неділя, з 9.00 до 14.00, з квітня по вересень</p><p>Адреса: Парковка Траст-банку, перетин 1-ої та Юніон стріт, центр</p>",

    'vendors_view_title' => "Постачальники",
    'vendors_view_machine_name' => "vendors",
    'vendors_view_path' => "vendors",

    'recipes_view_title' => "Рецепти",
    'recipes_view_machine_name' => "recipes",
    'recipes_view_path' => "recipes",
    'recipes_view_ingredients_label' => "Знайти рецепти за допомогою...",
    'recipes_view_block_display_name' => "Останні рецепти",
    'recipes_view_block_title' => "Нові рецепти",

    'recipes_view_title_translated' => "Recipes",
    'recipes_view_submit_button_translated' => "Apply",
    'recipes_view_ingredients_label_translated' => "Find recipes using...",

  ];

}
