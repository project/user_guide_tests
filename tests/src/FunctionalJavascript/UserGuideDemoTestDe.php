<?php

namespace Drupal\Tests\user_guide_tests\FunctionalJavascript;

/**
 * Builds the demo site for the User Guide, German, with screenshots.
 *
 * See README.txt file in the module directory for more information about
 * making screenshots.
 *
 * @group UserGuide
 */
class UserGuideDemoTestDe extends UserGuideDemoTestBase {

  /**
   * Non-override of UserGuideDemoTestBase::runList.
   *
   * If you want to run only some chapters, or want to make backups, change
   * the name of this variable (locally and temporarily) to $runList, and then
   * change 'skip' to one of the other values for each chapter you want to run.
   * See UserGuideDemoTestBase::runList for more information.
   *
   * @var array
   */
  protected $notRunList = [
    'doPrefaceInstall' => 'skip',
    'doBasicConfig' => 'skip',
    'doBasicPage' => 'skip',
    'doContentStructure' => 'skip',
    'doUserAccounts' => 'skip',
    'doBlocks' => 'skip',
    'doViews' => 'skip',
    'doMultilingualSetup' => 'skip',
    'doTranslating' => 'skip',
    'doExtending' => 'skip',
    'doPreventing' => 'skip',
    'doSecurity' => 'skip',
  ];

  /**
   * {@inheritdoc}
   */
  protected $demoInput = [
    'first_langcode' => "de",
    'second_langcode' => "en",

    'site_name' => "Wochenmarkt in Musterstadt",
    'site_slogan' => "Erzugnisse direkt vom Bauernhof",
    'site_mail' => "info@example.com",
    'site_default_country' => "DE",
    'date_default_timezone' => "Europe/Berlin",

    'home_title' => "Start",
    'home_body' => "<p>Willkommen auf dem Markt der Stadt - Ihrem Wochenmarkt in der Nachbarschaft!</p><p>Öffnungszeiten: Sonntags, 9:00 Uhr bis 14.00 Uhr, April bis Septembe</p><p>Location:  Parkplatz am Rathaus, Innenstadt Musterstadt.</p>",
    'home_summary' => "Öffnungszeiten und Standort des Wochenmarktes",
    'home_path' => "/start",
    'home_revision_log_message' => "Aktualisierte Öffnungszeiten",

    'home_title_translated' => "Home",
    'home_body_translated' => "<p>Welcome to City Market - your neighborhood farmers market!</p><p>Open: Sundays, 9 AM to 2 PM, April to September</p><p>Location: Parking lot of Trust Bank, 1st & Union, downtown</p>",
    'home_path_translated' => "/home",

    'about_title' => "Über",
    'about_body' => "<p>Der Wochenmarkt hat im April 1990 miot 5 Lieferanten angefangen.</p><p>Heute hat dr Wochenmarkt 100 Lieferanten und ungefähr 2000 Besucher pro Tag.</p>",
    'about_path' => "/über",
    'about_description' => "Geschichte des Wochenmarktes",

    'vendor_type_name' => "Lieferant",
    'vendor_type_machine_name' => "lieferant",
    'vendor_type_description' => "Informationen über den Lieferanten",
    'vendor_type_title_label' => "Name ds Lieferanten",
    'vendor_field_url_label' => "URL zur Website des Lieferanten",
    'vendor_field_url_machine_name' => "lieferanten_url",
    'vendor_field_image_label' => "Hauptbild",
    'vendor_field_image_machine_name' => "hauptbild",
    'vendor_field_image_directory' => "lieferanten",

    'vendor_1_title' => "Happy Farm",
    'vendor_1_path' => "/lieferanten/happy_farm",
    'vendor_1_summary' => "Happy Farmbeut Gemüse an, dass Sie lieben werden.",
    'vendor_1_body' => "<p>Happy Farm baut Gemüse an, dass Sie lieben werden.</p><p>Wir bauen Tomaten, Karotten und Rüben sowie eine Vielzahl von Salaten an.</p>",
    'vendor_1_url' => "http://happyfarm.com",
    'vendor_1_email' => "happy@example.com",

    'vendor_2_title' => "Sweet Honey",
    'vendor_2_path' => "/lieferanten/sweet_honey",
    'vendor_2_summary' => "Sweet Honey produziert das ganze Jahr über Honig in einer Vielzahl von Geschmacksrichtungen.",
    'vendor_2_body' => "<p>Sweet Honey produziert das ganze Jahr über Honig in einer Vielzahl von Geschmacksrichtungen.</p><p>Unsere Sorten umfassen Klee, Apfelblüte und Erdbeere.</p>",
    'vendor_2_url' => "http://sweethoney.com",
    'vendor_2_email' => "honey@example.com",

    'recipe_type_name' => "Rezept",
    'recipe_type_machine_name' => "rezept",
    'recipe_type_description' => "Von einem Lieferanten eingereichtes Rezept",
    'recipe_type_title_label' => "Recipe name",
    'recipe_field_image_directory' => "rezepte",
    'recipe_field_ingredients_label' => "Zutaten",
    'recipe_field_ingredients_machine_name' => "zutaten",
    'recipe_field_ingredients_help' => "Geben Sie Zutaten ein, nach denen Besucher der Website suchen könnten",
    'recipe_field_submitted_label' => "Eingereicht von",
    'recipe_field_submitted_machine_name' => "eingereicht_von",
    'recipe_field_submitted_help' => "Wählen Sie den Lieferanten aus, der dieses Rezept eingereicht hat",

    'recipe_field_ingredients_term_1' => "Butter",
    'recipe_field_ingredients_term_2' => "Eier",
    'recipe_field_ingredients_term_3' => "Milch",
    'recipe_field_ingredients_term_4' => "Karotten",

    'recipe_1_title' => "Grüner Salat",
    'recipe_1_path' => "/rezepte/grüner_salat",
    'recipe_1_body' => "Schneiden Sie Ihr Lieblingsgemüse klein und geben Sie es in eine Schüssel.",
    'recipe_1_ingredients' => "Karotten",

    'recipe_2_title' => "Frische Karotten",
    'recipe_2_path' => "/rezepte/karotten",
    'recipe_2_body' => "Servieren Sie zum Abendessen bunte Karotten auf einem Teller.",
    'recipe_2_ingredients' => "Karotten",

    'image_style_label' => "Extramedium (300x200)",
    'image_style_machine_name' => "extra_medium_300x200",

    'hours_block_description' => "Block mit Öffnungszeiten und Standort",
    'hours_block_title' => "Öffnungszeiten und Standort",
    'hours_block_title_machine_name' => "oeffnungszeiten_standort",
    'hours_block_body' => "<p>Öffnungszeiten: Sonntags, 9:00 Uhr bis 14.00 Uhr</p><p>Standort:  Parkplatz am Rathaus, Innenstadt Musterstadt.</p>",

    'vendors_view_title' => "Lieferanten",
    'vendors_view_machine_name' => "lieferanten",
    'vendors_view_path' => "lieferanten",

    'recipes_view_title' => "Rezepte",
    'recipes_view_machine_name' => "rezepte",
    'recipes_view_path' => "rezepte",
    'recipes_view_ingredients_label' => "Rezeptzutaten",
    'recipes_view_block_display_name' => "Neueste Rezepte",
    'recipes_view_block_title' => "Neue Rezepte",

    'recipes_view_title_translated' => "Rezepte",
    'recipes_view_submit_button_translated' => "Anwenden",
    'recipes_view_ingredients_label_translated' => "Rezept mit Zutat finden ...",

  ];

}
