This directory contains images to be used in making screen shots for the User
Guide. This file lists copyright and license information for these files.

The files in this directory have various attributions and copyrights, as noted
below. Files that are listed as "CC BY SA 2.0" can be used in accordance with
the Creative Commons License, Attribution-ShareAlike 2.0 (CC BY-SA 2.0) -- see
https://creativecommons.org/licenses/by-sa/2.0/. All other files are in the
public domain.


COPYRIGHTS

If you use all or part of any of the images, include the following attribution
information (shown in AsciiDoc format):


honey_bee.jpg

The honey bee image is a public domain image by
https://en.wikipedia.org/wiki/en:User:Severnjc[John Severns] at the
https://en.wikipedia.org/wiki/en:[English Wikipedia project] via
https://commons.wikimedia.org/wiki/File:European_honey_bee_extracts_nectar.jpg[wikimedia
Commons].


farm.jpg

The farm image is a public domain image by Xianmin Chang via
https://commons.wikimedia.org/wiki/File:Bere%26ModernBarley.jpg[Wikimedia Commons].


salad.jpg

The salad image is a public domain image by http://www.goodfreephotos.com[Yinan
Chen] via https://commons.wikimedia.org/wiki/File:Gfp-salad.jpg[Wikimedia
Commons]


carrots.jpg

The carrot image is public domain image
https://www.ars.usda.gov/is/graphics/photos/nov04/K11611-1.htm[K11611-1] by
Stephen Ausmus of the
https://en.wikipedia.org/wiki/Agricultural_Research_Service[Agricultural
Research Service of the United States Department of Agriculture]
via https://commons.wikimedia.org/wiki/File:Carrots_of_many_colors.jpg[Wikimedia
Commons]


AnytownFarmersMarket.png

The Anytown Farmers Market logo image is a CC BY-SA 2.0 licensed illustration by
Justin Harrell of https://drupalize.me/[Drupalize.Me].
