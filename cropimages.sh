#!/bin/bash

# Run this script from the directory of images to be cropped

for fname in $(find . -type f -name "*.png")
do
    echo "Cropping $fname"
    convert $fname -trim +repage $fname
done
