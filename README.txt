OVERVIEW
--------

This module does not do anything directly. All it contains is tests that:
- Verify that the user interface text that is used in the User Guide and the
  steps for the tasks are present and work.
- Generate screen capture images
- Generate database dumps and files directories (known as "backups") that you
  can use to clone the demo site that the User Guide builds, as it would be at
  the end of each chapter.


EXCLUSIONS
----------

A list of images that could not be automated, and therefore need to be generated
manually, can be found in the source/en/images/README.txt file in the main
User Guide project directory.

There are also some pages in the User Guide that are difficult to test using
this automated test framework, because they involve steps on drupal.org. There
may be a test added for them later, but for now, these pages should be manually
tested periodically, to make sure the text in the User Guide for task steps
matches the current drupal.org site:

- extend-manual-install
- extend-module-find
- extend-module-install
- extend-theme-find
- extend-theme-install
- install-prepare
- security-update-module
- security-update-theme


RUNNING TESTS ON DRUPALCI
-------------------------

If you are a maintainer of the User Guide Tests project, you can run the tests
on the DrupalCI infrastructure by following these steps:

1. Log in to Drupal.org and go to the User Guide Tests project page:
   https://www.drupal.org/project/user_guide_tests

2. Click "Automated testing".

3. Find the latest branch of the project, such as 8.x-6.x-dev, and click
   "Add test / retest" under that branch.

4. Choose these values and click "Save & queue":
   Environment: PHP 7.4 & MySQL 5.7
   Core: Stable release
   Schedule: Run once

5. Wait for the tests to finish running.


TEST RESULTS, IMAGES, AND BACKUPS
---------------------------------

1. Log in to Drupal.org and go to the User Guide Tests project page:
   https://www.drupal.org/project/user_guide_tests

2. View the latest GitLabCI Pipeline for the branch you are interested in. Here
   you can see if the tests passed or failed, and what error messages they
   generated.

3. Click the "Tests" tab, and then select phpunit.

   You'll see a list of all the tests that ran, one for each enabled language.

   You can browse the artifacts for the tests to find helpful debugging
   information. You'll see a list of several directories called things like
   "test29567894"; each contains screenshots and backups for a language's tests.

   There will also be a lot of links called things like
   "Drupal_Tests_user_guide_tests...", which are HTML output from each page
   visited by the internal browser in the test. The link names also tell you
   which directory corresponds to which language. For instance, the link whose
   URL ends in: _UserGuideDemoTestCa-100-72583804.html tells you that the CA
   (Catalan) language is in the test72583804 directory.

4. If you need to debug a test, look at the highest-numbered HTML link for
   that language, along with the error message that you can see on the test
   output page.

5. If you want to download new backups and images for a particular language,
   or all languages, first download a copy of the test artifacts. Then look in
   the web/sites/simpletest/browser_output/ directory for directories like
   test123645/, each subdirectory is the files for a specific language. The
   subdirectory will contain a file like lang-en.txt that indicates the language
   for the files in that directory.

6. Subdirectory "backups" in each language output directory has the backups for
   that language. Use the "copybackups.sh" script to copy them into the backups
   subdirectory in this project for that language.

7. Subdirectory "screenshots" in each language output directory has the
   uncropped screenshot images for that language. Use the "cropimages.sh" script
   to crop them; you will need to have the ImageMagick command "convert"
   available on your computer. After cropping, you can use the
   "compareimages.sh" script to compare them with the previous versions, which
   are located in the source/LL/images directory in the User Guide project,
   where LL is the language code. Images that have changed materially can
   then be committed to that project.


SETTING UP TO RUN TESTS MANUALLY
--------------------------------

You can run the tests on your own local system using Drupal Core's PHPUnit test
framework from the command line. This section details how to set up the tools;
some steps may need to be repeated if you update software on your local
computer. Here are the steps:

1. Install the Chrome or Chromium browser, if you do not already have it
   installed.

2. Download the Drupal release that you want to make screenshots for.
   (Drupal 8.0.2, 8.1.0, etc. -- make sure it is the latest actual release, not
   a development branch, for purposes of screenshots).

3. Apply patches for core issue(s):

   https://www.drupal.org/project/drupal/issues/2905295
   At this time, the issue has no official patch. But it can be gotten around
   by applying this work-around patch:
   https://www.drupal.org/files/issues/2019-02-26/2905295-work-around.patch

4. If you do not have Composer
   installed, see https://getcomposer.org/

5. If you installed from a tar.gz download, grab the require-dev part of
   composer.json from a dev download or the Git repository, and put it into
   your composer.json file. Run "composer update" to update the lock file
   and install the development files.

6. From your Drupal root directory, run the following command:

   composer require drupal/user_guide_tests

   This will download the User Guide Tests module, the Honey theme, and the
   Admin Toolbar and Backup & Migrate modules.

7. If you prefer to run tests from your local Git repository copy of the User
   Guide Tests module, remove the downloaded modules/contrib/user_guide_tests
   and replace it with a symbolic link to your local git repo of the module.

8. Follow the steps in core/tests/README.md to set up the testing environment,
   including the parts that are specific to running tests that use
   chromedriver and WebDriverTestBase. If you define any output
   locations in the phpunit.xml file, you will need to make those directories
   and make sure they are writeable. Similarly, make sure the database you are
   using exists and the database user has access.

9. Make sure that your sites/default directory is writeable by the www-data
    user (or the user you use to run the tests).

RUNNING THE TESTS MANUALLY
--------------------------

Once you are set up for testing, you can run the tests for a particular language
as follows:

1. Start chromedriver and keep it running:

   chromedriver --port=4444

2. Find the test file for the language you want to run. The tests for each
   language are in the tests/src/FunctionalJavascript directory under this
   directory.

3. Optionally, edit the test file to make it run just a subset of the
   screenshots and tests, and to enable/disable the saving of database/file
   backups. To do this, find the member variable $notRunList in the test file,
   change its name to $runList, and change 'skip' to another value for each
   section you want to run. The values are documented in the base class
   UserGuideDemoTestBase.php.

   You may also want to override the $doCrop member variable from the
   ScreenshotTestBase base class. If it is set to TRUE, the test will crop
   images automatically. The default is FALSE because cropping doesn't work
   in many PHP environments. You can run the test called DrupalScreenshotTest
   to see if the cropping process works.

4. Run the test for the language of interest with a command like this, from the
   core directory under your Drupal root:

   sudo -u www-data ../vendor/bin/phpunit -v -c /path/to/phpunit.xml \
   ../modules/contrib/user_guide_tests/tests/src/FunctionalJavascript/UserGuideDemoTestEn.php

   If your web user is something other than www-data, modify the command
   appropriately, and change the language near the end of the command if
   necessary. You will also need to change the path to your phpunit.xml file.

5. Assuming the test run succeeds, you should see some output that tells you
   where the backups and screenshot files have been stored.


RESTORING BACKUPS
-----------------

This Git repository contains file and database backups from running the tests
for each language, for each chapter. You can use them to set up a site that
contains the output of the User Guide steps, at the end of each chapter. Find
backups in subdirectory "backups" under this directory, organized by language
and then by chapter.

Note that if you restore a database backup, the database table prefix has been
set to 'generic_simpletest_prefix'. You can either set your Drupal site to use
this database prefix in your settings.php file, or you could do a search/replace
on the database backup file before you import it.

The file backups contain the contents of the sites/default/files directory.


MAINTAINER/DEVELOPER NOTES
--------------------------

Making a test class for a new language:
- Extend the base UserGuideDemoTestBase class, and name it UserGuideDemoTestLL,
  where LL is the language code.
- Override the $demoInput member variable, translating the input into the
  target language. Note that most of the text should not contain
  ' characters, as this will result in an error when generating the screen
  shots. If you have a spreadsheet with the array keys for $demoInput in
  column A, and the translated text in column C, you can use this formula
  to generate the array in row 2 (and then copy to the other rows):
     =IF(A2 <> "","'"&A2&"' => """&C2&""",","")
  Then copy this column of output into the $demoInput array in your
  new class.
- Add PO files to the translations directory (see README.txt file there for
  instructions).

Troubleshooting failing tests:
- These tests are based on WebDriverTestBase, which uses Chromedriver to
  automatically drive a Chrome or Chromium browser.
- If you are having problems at a particular line of a test, you can insert
  the following line just above it to stop the test and do things manually in
  the browser (including looking at the dblog report, etc.):
    $this->stopTheTestForDebug();
  Once you are done, close the browser window. The test will abort.
- The browser in tests is sensitive to scrolling -- if it has scrolled down too
  far, it may not find form fields to fill in, text in asserts, etc. You can
  insert
    $this->scrollWindowUp();
  in a test to scroll the window back to the top and avoid this problem.
- The browser has JavaScript and Ajax capabilities, so it should behave mostly
  like what you see in your own browser on a Drupal site. But if you trigger
  an Ajax behavior, you need to wait for it to finish before going on with the
  next step in the test -- if you don't, you may have weird-looking test failure
  messages having something to do with Curl calls. To wait for Ajax to finish,
  insert the following line in the test:
    $this->assertSession()->assertWaitOnAjaxRequest();

TRANSLATION FILES
-----------------

The official download of the Farsi translation for Drupal core is missing some strings that are required for the test to work. The strings have suggested translations, but those are not yet part of the official .po file until they are approved. In the meantime, you can join the Persian/Farsi translation team on localize.drupal.org and use the Export tool to download a .po file that contains the suggestions.
