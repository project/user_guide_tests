#!/bin/bash

# Run this script from a directory containing cropped image files from a test
# run. The argument is the directory containing previous versions of the
# images. The images will be compared using the ImageMagick compare utility,
# and the output of the comparisons will be stored in a new tmp subdirectory,
# which must not currently exist. You can look at the comparison outputs to
# see what has changed.

mkdir tmp

for fname in $(ls *.png)
do
    echo "Comparing $fname"
    convert $fname -gravity Northwest -background white -extent 1500x1500! new.png
    convert $1/$fname -gravity Northwest -background white -extent 1500x1500! old.png

    compare old.png new.png tmp/$fname
    rm old.png new.png
done
